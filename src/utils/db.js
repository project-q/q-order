const mongo = require('mongodb')

const dbName = 'q-order'

const state = {
  db: null,
}

exports.connect = function (url, done) {
  if (state.db) return done()

    mongo.MongoClient.connect(url, (err, db) => {
        if (err) return done(err);
        db = db.db(dbName)
        state.db = db;
        done();
    })
}

exports.get = function () {
  return state.db
}

exports.collection = function (collection) {
  return state.db.collection(collection)
}

exports.close = function (done) {
  done = done || function () { }
    if (state.db) {
    state.db.close((err, result) => {
            state.db = null;
            state.mode = null;
            done(err);
        })
  }
}
