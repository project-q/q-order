const faye = require('faye')
const shortid = require('shortid')
const db = require('../utils/db')
const { request } = require('graphql-request')
const client = new faye.Client('http://localhost:8000/')



module.exports = {
  Query: {
    getOrder: (_parent, args, _context, _info) => {
      const { orderId } = args
      return db.collection('orders').findOne({ _id: orderId })
    },
    getOrdersByConsumer: async (_parent, args, context, _info) => {
      const { consumerId } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == consumerId || user.accountType == 'ADMIN') {
        const order = await db.collection('orders').find({ consumer: consumerId }).toArray()
        return order
      } else {
        return null
      }
    },

    getOrdersByProducer: async (_parent, args, context, _info) => {
      const { producerId } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == producerId || user.accountType == 'ADMIN') {
        const order = await db.collection('orders').findOne({ producer: producerId })
        return order
      } else {
        return null
      }
    },
  },
  Mutation: {
    createOrder: async (_parent, args, context, _info) => {
      const { orderInput } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == orderInput.consumer || user.accountType == 'ADMIN') {
        orderInput._id = shortid.generate()

        const consumerQuery = `
        query getAccount{
          getAccount(_id: "${orderInput.consumer}"){
            address{
              street_name
              street_number
              city
              zip_code
              country
            }
          }
        }
        `
        const producerQuery = `
        query getAccount{
          getAccount(_id: "${orderInput.producer}"){
            address{
              street_name
              street_number
              city
              zip_code
              country
            }
            ...on Producer{
              taxId
            }
          }
        }
        `

        const consumerAddress = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', consumerQuery).then(data => {
          const { address } = data.getAccount
          return address
        })

        const producerAddress = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', producerQuery).then(data => {
          const { address } = data.getAccount
          return address
        })

        const producerTaxId = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', producerQuery).then(data => {
          const { taxId } = data.getAccount
          return taxId
        })

        var totalAmountOfOrder = 0

        orderInput.products.forEach(element => {
          totalAmountOfOrder += element.price
        });

        orderInput.bill = {
          consumerAddress: consumerAddress,
          producerAddress: producerAddress,
          billNumber: shortid.generate(),
          issueDate: Date.now(),
          dueDate: Date.now(),
          total: totalAmountOfOrder,
          products: orderInput.products,
          shippingPrice: 0,
          tax: 0,
          taxId: producerTaxId
        }

        await db.collection('orders').insertOne(orderInput)

        client.publish('/addedOrder', {
          text: 'Hello world',
          json: orderInput,
        })
        return orderInput
      } else {
        return null
      }
    },
    changeOrderStatus: async (_parent, args, context, _info) => {
      const { orderId, orderStatus } = args.statusInput
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == orderId || user.accountType == 'ADMIN') {
        const updateOrder = await db.collection('orders').findOneAndUpdate({ _id: orderId }, { $addToSet: { 'orderStatus': orderStatus } }, { returnOriginal: false })
        return updateOrder.value
      } else {
        return null
      }
    },


    /**
     * TODO: implment changeOrderStatus with faye pubsub
     */
  },
}
// DUMMY Data for TESTING!!!
/* mutation addOrder{
  createOrder(orderInput:{
    consumer: {consumerId:"12345"},
    producer: {producerId:"abcde"},
    products:[{id: "1", name:"Apfel", amount:5, tax: 5, price:5}],
    total: 15,
    pickupStation:{
      locationId: "qwertz",
      pickupDeadline: "now",
      name: "Hier",
      pickupType: PRODUCER,
      address:{
        street_name:"here",
        street_number: "5",
        city:"Dort",
        zip_code:12345,
        country: "Indien"
      },
      openingHours:{
        is_closed: false,
        start: "10",
        end: "20"
      },
    }
    date:"now",
    orderStatus: {
      status: ORDERED,
      data: "now"
    },
    paymentMethod:PAYPAL,
    bill:{
      consumerAddress:{
        street_name:"here",
        street_number: "5",
        city:"Dort",
        zip_code:12345,
        country: "Indien"
      },
      producerAddress:{
        street_name:"here",
        street_number: "5",
        city:"Dort",
        zip_code:12345,
        country: "Indien"
      },
      billNumber:5,
      issueDate: "now",
      dueDate: "later",
      total:15,
      products:[{id: "1", name:"Apfel", amount:5, tax: 5, price:5}],
      shippingPrice:5,
      tax:5,
      taxId: "2"
    }
  }){
    total
  }
} */
